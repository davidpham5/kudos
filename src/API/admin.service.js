import axios from 'axios'

function AdminService (token) {
  const config = {headers: {
    'Authorization': `Bearer ${token}`
  }}

  const getEmojis = () => {
    const url = 'qudos/slack/emoji'
    return axios.get(url, config)
      .then(resp => resp)
      .catch(error => error);
  }

  const updateTeam = ({data}) => {
    const url = 'qudos/team'
    return axios.get(url, config, data)
      .then(resp => resp)
      .catch(error => error);
  }

  const model = {
    getEmojis: getEmojis,
    updateTeam: updateTeam
  }

  return model
}

export default AdminService;