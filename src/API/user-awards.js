const userAwards = [
  {
    username: "freddy",
    fullName: "Freddy Montano",
    sentMost: true,
    createdAt: 593931199,
    kudoCount: 5,
    sentTotal: 128,
    receivedTotal: 536,
    rank: 3,
    mostSentBy: {
      username: "dpham",
      fullName: "David Pham",
      description: "Most Sent By",
      total: 23
    },
    mostReceivedBy: {
      username: "bsun",
      fullName: "Benjamin Sun",
      description: "Most Received By",
      total: 18
    },
    timeFrame: {
      start: 593931199,
      end: 593931200
    }
  }
];

class UserAwardsAPI {
  static getAllUserAwards () {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], userAwards))
      }, 1000)
    })
  }
}

export default UserAwardsAPI