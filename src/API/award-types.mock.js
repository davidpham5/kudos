const awards = [
  {
    id: 1,
    label: 'Most Kudos',
    description: 'received the most kudos',
    winner: 'dpham'
  },{
    id: 2,
    winner: 'dpham',
    label: 'Funniest',
    description: 'did you make someone laugh'
  },{
    id: 3,
    winner: 'dpham',
    label: 'Blue Wave',
    description: 'underdog flips enough people to win channel'
  },{
    id: 4,
    winner: 'freddy',
    label: 'You Made Your Point',
    description: 'refused to accept kudos'
  },{
    id: 5,
    winner: 'freddy',
    label: 'Be Gentle It\'s My First Time',
    description: 'expected to receive a harsh kudo, but got a sweet one'
  },{
    id: 6,
    winner: 'diCaps',
    label: 'Rage Quit',
    description: 'unexpectedly leaves channel'
  }, {
    id: 7,
    winner: 'dpham',
    label: 'Beastie Boys',
    description: 'probably one of the most underrated bands ever.'
  }, {
    id: 8,
    winner: 'streeter',
    label: 'stanger things',
    description: 'being weird.'
  }
]

class AwardsAPI {
  static getAllAwards () {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], awards))
      }, 1000)
    })
  }

  static saveAward (award) {
    award = Object.assign({}, award);
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const mimDescriptionLength = 3;

        if (award.description.length < mimDescriptionLength) {
          reject('come on, be more descriptive.');
        }

        if (award.winner === '') {
          reject('name a winner')
        }

        resolve(award)
      }, 2000)
    });
  }

  static deleteAward(awardId) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const indexOfAwardToDelete = awards.findIndex((award) => {
          return award.id === awardId;
        });
        awards.splice(indexOfAwardToDelete, 1);
        resolve();
      }, 1000);
    });
  }
}

export default AwardsAPI;