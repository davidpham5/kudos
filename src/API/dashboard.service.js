import axios from 'axios'

const DashboardService = {
  getAllAwards: service().getAllAwards
}

function service () {
  //const url = 'http://ec2-18-218-44-20.us-east-2.compute.amazonaws.com:3002/dashboard';
  const url = '/qudos/dashboard'
  const getAllAwards = (token) => {
    const config = {headers: {
      'Authorization': `Bearer ${token}`
    }}

    return axios.get(url, config)
      .then(resp => resp)
      .catch(error => error);
  }
  const model = {
    getAllAwards: getAllAwards
  }

  return model
}

export default DashboardService;