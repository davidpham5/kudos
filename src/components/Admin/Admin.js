import React, { Component } from 'react';
import AdminService from '../../API/admin.service'
import { Media, Tab, Tabs, Button } from 'react-bootstrap';

const TOKEN = process.env.REACT_APP_TOKEN;

class Admin extends Component {
  state = {
    emojis: [],
    key: 'default_emojis',
    searchDefault: ''
  }

  async componentDidMount () {
    const resp = await AdminService(TOKEN).getEmojis();
    const formatLabels = emojiType => emojiType.map(emoji => Object.assign(emoji, {label: emoji.name.replace(/_/g, " "), checked: false}));

    const defaultEmojies = formatLabels(resp.data.defaultEmojies);
    const customEmojies = formatLabels(resp.data.customEmojies);

    try {
      this.setState({emojis: {defaultEmojies, customEmojies}})
    } catch (error) {
      console.log(error)
    }
  }

  updateEmojiSetting = (e, item) => {
     return {...item, checked: true}
  }

  renderList = (items) => {
    return (items &&
       items.map((item, index) => (
        <Media key={index} className="emojis-default">
          <a role="button" onClick={(e) => this.updateEmojiSetting(e, item)}>
            <Media.Left>
              {item.url &&
                <img className="emoji-icn-as-img" src={item.url} alt={item.label}/>
              }
              <span className="emoji-icn" role="img" dangerouslySetInnerHTML={{__html: item.char}}></span>
            </Media.Left>
            <Media.Body>
              <Media.Heading>
                {item.label}
                {item.checked && <span className="checked"></span>}
              </Media.Heading>
            </Media.Body>
          </a>
        </Media>
      ))
    )
  }

  filterList = (e, emojiType) => {
    const { value } = e.target
    const filteredDefault = value
      ? [...emojiType].filter(item => item.label.toLowerCase().indexOf(value) !== -1)
      : [...emojiType]

    return this.setState({searchDefault: value, filteredDefault})
  }

  clearList = () => {
    return this.setState({searchDefault: '', filteredDefault: this.state.emojis.defaultEmojies });
  }

  render () {
    const { emojis, filteredDefault, searchDefault } = this.state;
    const { defaultEmojies, customEmojies } = emojis;
    const defaultEmoji = (filteredDefault && filteredDefault) || defaultEmojies;

    return (
      <div className="content-grid">
        {/* <Picker set='apple' title="dirty pants  " /> */}
        <Tabs
          activeKey={this.state.key}
          onSelect={key => this.setState({ key })}
          id="emojisTabs">
          <Tab eventKey="default_emojis" title="Default Emojs">
            <div className="content-emojis card">
              <div className="form-group search-input">
                <label htmlFor="filterEmojis">Search Emojis</label>
                <input
                  type="search"
                  onChange={(e) => this.filterList(e, defaultEmojies)}
                  value={searchDefault}/>
                <button onClick={this.clearList} className="btn btn-default">&times;</button>
              </div>
              <div className="grid-list emojis-list">
                {defaultEmoji && this.renderList(defaultEmoji)}
              </div>
            </div>
          </Tab>
          <Tab eventKey="custom_emojis" title="Custom Emojs">
            <div className="content-emojis card">
              <div className="grid-list emojis-list">
                {customEmojies && this.renderList(customEmojies)}
              </div>
            </div>
          </Tab>
        </Tabs>
      </div>
    )
  }
}

export default Admin;
