import React, { Component } from 'react';
import DashboardService from '../API/dashboard.service';
import AwardsAPI from '../API/award-types.mock';
import UserAwardsAPI from '../API/user-awards';
import UserAward from './Awards/user-award';
//import ListAwards from './Awards/list-awards';
import TableAwards from './Awards/table-awards';
const TOKEN = process.env.REACT_APP_TOKEN;
class Dashboard extends Component {
  constructor (props) {
    super(props)
    this.state = {
      content: '',
      emoji: '',
      allAwards: [],
      userAward: []
    }
    this.getDashboardData = this.getDashboardData.bind(this);
    this.getUserAward = this.getUserAward.bind(this);
  }

  async getDashboardData () {
    const response = await DashboardService.getAllAwards(TOKEN);
    const { data } = response && response;
    const { team } = data

    try {
      this.setState({ content: data, team: team })
    } catch (error) {
      throw new Error(error)
    }
  }


  async getUserAward () {
    const [ userAward ] = await UserAwardsAPI.getAllUserAwards()

    try {
      this.setState({userAward})
    } catch (error) {
      throw new Error(error)
    }
  }

  async componentDidMount () {
    this.getDashboardData()
    this.getUserAward()
    const allAwards = await AwardsAPI.getAllAwards()

    try {
      this.setState({allAwards: allAwards})
    } catch (error) {
      throw new Error(error)
    }
  }


  render () {
    const {content, team, userAward} = this.state;

    return (
      <div className="content-ly">
        <div className="overview">
          <UserAward userAward={userAward} />
        </div>
        <div className="content-grid">
          <h2 className="section-header">Awards Given</h2>
          <div className="all-awards">
            {content &&
              <TableAwards awards={content.allAwards} team={team}/>
            }
          </div>

          {/* <div className="grid-list">
            {content &&
              <ListAwards awards={content.allAwards} team={team}/>
            }
          </div> */}
        </div>
      </div>
    )
  }
}

export default Dashboard
