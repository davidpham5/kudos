import React, { Component } from 'react';
import DashboardService from '../../API/dashboard.service';
const TOKEN = process.env.REACT_APP_TOKEN;

class UserAvatar extends Component {
  constructor (props) {
    super (props);
    this.state = {
      user: {}
    }
    this.getDashboardData = this.getDashboardData.bind(this);
  }

  async getDashboardData () {
    const response = await DashboardService.getAllAwards(TOKEN);
    const { user } = response.data;

    try {
      this.setState({user})
    } catch (error) {
      throw new Error(error)
    }
  }

  async componentDidMount () {
    this.getDashboardData();
  }

  render () {
    const {user} = this.state;
    const {imgStyle} = this.props;
    return (
      <div>
        {user.avatar_512 &&
          <img src={user.avatar_512} className={imgStyle} alt="thumbnail" />
        }
      </div>
    )
  }
}

export default UserAvatar;