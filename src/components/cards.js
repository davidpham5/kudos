<div className="card">
        <div className="card-header">
         
        </div>
        <div className="card-body">
          <h1 className="card-user-name">{ userAward.fullName }</h1>
          <div className="rank-container">
            {userAward.rank &&
            <div className="rank">
              🌟 {this.formatRank(userAward.rank)}
            </div>
            }
          </div>

          <div className="totals-container">
            <ul className="totals-list">
              <li>Sent <span className="totals">{userAward.sentTotal}</span></li>
                <li>Received <span className="totals">{userAward.receivedTotal}</span></li>
              </ul>
          </div>
        </div>
      </div>