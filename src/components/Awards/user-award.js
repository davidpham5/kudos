import React, { Component } from 'react';
//import UserAvatar from '../Avatars/user-avatars';
class UserAward extends Component {
  constructor (props) {
    super(props);

    this.formatRank = this.formatRank.bind(this);
  }

  formatRank (rank) {
    var ranked = '';
    switch (rank) {
      case 1:
        ranked = `${rank}st`;
        break;
      case 2:
        ranked = `${rank}nd`;
        break;
      case 3:
        ranked = `${rank}rd`
        break;
      default:
       ranked = `${rank}th`;
    }
    return ranked
  }

  render() {
    const {userAward} = this.props;

    return (
      <div className="overview-main">
        <div className="overview-body">
          <div className="rank-container">
            {userAward.rank &&
            <div className="totals-rank">
              <div className="totals-label">Rank</div>
              <div className="rank">
                <span role="img" aria-label="star">🌟</span> {this.formatRank(userAward.rank)}
              </div>
              <button className="btn-overview">
                View
                <span className="icon-container">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.95 10.707l.707-.707L8 4.343 6.586 5.757 10.828 10l-4.242 4.243L8 15.657l4.95-4.95z"/></svg>
                </span>
              </button>
            </div>
            }
          </div>
          <div className="totals-sent">
            <div className="totals-label">Sent</div>
            <div className="totals">{userAward.sentTotal}</div>
            <button className="btn-overview">
              View
            <span className="icon-container">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.95 10.707l.707-.707L8 4.343 6.586 5.757 10.828 10l-4.242 4.243L8 15.657l4.95-4.95z"/></svg>
                </span>
            </button>
          </div>
          <div className="totals-receive">
            <div className="totals-label">Received</div>
            <div className="totals">{userAward.receivedTotal}</div>
            <button className="btn-overview">
              View
              <span className="icon-container">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.95 10.707l.707-.707L8 4.343 6.586 5.757 10.828 10l-4.242 4.243L8 15.657l4.95-4.95z"/></svg>
                </span>
            </button>
          </div>
        </div>
      </div>
    )
  }
}

export default UserAward;
