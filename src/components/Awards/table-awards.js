import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import Moment from 'react-moment';
class TableAwards extends Component {
  render() {
    const { awards } = this.props;
    return (
      <Table striped>
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Date</th>
            <th>Gave</th>
            <th>Emoji</th>
          </tr>
        </thead>
        <tbody>
          {
            awards.map((award) => {
              return (
                <tr key={award.id}>
                  <td className="text-center"> <img width={40} height={40} src={award.sender.avatar_512} className="rounded" alt="thumbnail" /></td>
                  <td>{award.sender.slack_display_name}</td>
                  <td>
                    <Moment format="MMM DD, YYYY">{ award.created_at }</Moment>
                  </td>
                  <td className="text-left">{award.amount}</td>
                  <td><span role="img" aria-label="banana">🍌</span></td>
                </tr>
              );
            })
          }
        </tbody>
      </Table>
    )
  }
}

export default TableAwards;