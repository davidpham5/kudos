import React, { Component } from 'react';
import { Media } from 'react-bootstrap';

class ListAwards extends Component {
  render () {
    const { awards, team } = this.props;

    return awards.map((award) => {
      return (
        <Media key={award.id}>
          <Media.Left>
            <img width={64} height={64} src={award.sender.avatar_512} className="rounded" alt="thumbnail" />
          </Media.Left>
          <Media.Body>
            <Media.Heading>{award.sender.slack_display_name}</Media.Heading>
            <p>
              Gave {award.amount > 5 ? `${award.amount} (Holy Moly)!` : award.amount} {team.emoji_display_name}{award.amount > 1 ? 's' : ''} to {award.recipient.slack_display_name}
            </p>
            <div className="meta">{award.created_at}</div>
          </Media.Body>
        </Media>
      )
    })
  }
}

export default ListAwards;