import React from "react";
import { Switch } from 'react-router-dom';
import { RouteProvider } from './components/RouteProvider';

import Dashboard from './components/Dashboard';
import Admin from './components/Admin/Admin';

 function Routes ({bindings}) {
  return (
    <Switch>
      <RouteProvider path='/' exact component={ Dashboard } props={ bindings } />
      <RouteProvider path='/admin' exact component={ Admin } props={ bindings } />
      { /* Finally, catch all unmatched routes */ }
      {/* 404 View */}
    </Switch>
  )
}

export default Routes;