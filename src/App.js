import React, { Component } from 'react';
import './styles/App.css';

import Sidebar from './components/Sidebar';
import UserAvatar from './components/Avatars/user-avatars';
import Routes from './Routes';
import { Link } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="">
          <header className="App-header">
            <header className="header--branding">
              <div className="branding">Qudos</div>
            </header>
            <nav>
              <ul>
                <li>
                  <Link to='/'>Dashboard</Link>
                </li>
                <li>
                  <Link to='/admin'>Admin</Link>
                </li>
              </ul>
            </nav>
            <div className="user-account">
              <UserAvatar imgStyle='rounded' />
            </div>
          </header>
          <div className="main">
            <Sidebar />
            <section className="content">
              <Routes />
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
